# Семинар 02.04.2019

# Виды типов

Haskell позволяет определять параметрические типы, такие как `Maybe a` или
`Either a b`. Сами по себе слова `Maybe` и `Either` не называют тип,
а являются конструкторами типов от одного и двух аргументов соответственно.
Невозможно объявить значение `m :: Maybe`, нужно передать конструктору
`Maybe` какой-то тип, например `mInt :: Maybe Int`. Можно рассматривать
`Maybe` как функцию над типами, которая принимает тип и возвращает тоже тип.
Аналогично и для `Either`, только этому конструктору нужно передать два
аргумента. Есть и "константные" конструкторы типов от нуля аргументов,
такие как `Int`, `Float`, `Char` и т.д. К ним также будут относиться
`Maybe Char` или `Either String Int`.

Чтобы отличать такие объекты, вводится "тип типов", который называется "вид"
(kind). На Haskell это записывается так:

```haskell
Int :: *
Maybe :: * -> *
Either :: * -> * -> *
```

На семинаре определяли виды следующих типов:

```haskell
data A a = A a
data B a = B a (B a)
data D a = D
-- A, B, D :: * -> *

data I f a = I (f a)
-- I :: (* -> *) -> * -> *
data J f g a = J (f (g a))
-- J :: (* -> *) -> (* -> *) -> * -> *
data K a f b = K a
-- K :: * -> * -> * -> *
data M f a = MP a | MF (f (M f a))
-- M :: (* -> *) -> * -> *
data O t m a = O (t m a)
-- O :: O :: (* -> * -> *) -> * -> * -> *
```

# Функторы

Типы из класса `Functor` можно рассматривать как контейнеры, содержащие
значения, причём мы можем преобразовать весь контейнер, если умеем
преобразовывать значения внутри него:

```haskell
class Functor f where
  fmap :: (a -> b) -> f a -> f b
```

Функтором может быть только тип вида `* -> *`.

На реализацию `fmap` накладываются дополнительные ограничения, которые,
к сожалению, компилятор не умеет проверять автоматически:

1. `fmap id` = `id`
2. `fmap (g . f)` = `fmap g . fmap f`

На семинаре мы определили реализацию класса `Functor` для следующих
типов:

```haskell
data AssocList k v = AssocList [(k, v)]
instance Functor (AssocList k) where
  fmap f (AssocList l) = AssocList $ map (\(k, v) -> (k, f v)) l

data RoseTree a = RoseTree a [RoseTree a]
instance Functor RoseTree where
  fmap f (RoseTree v rs) = RoseTree (f v) (map (fmap f) rs)

data Grid a = Grid ((Int, Int) -> a)
instance Functor Grid where
  fmap f (Grid g) = Grid (\x -> f (g x))
```

# Домашнее задание

1. Определить вид типов:

```haskell
data C a b = C a (C b a) | CN
data E = E
data F a b = F (a -> b)
data G a b = G1 (a -> b) | G2 (b -> a)
data H a b = H ((b -> a) -> a)
data L f = L f (L f)
data N r f a = N ((a -> f r) -> f r)
```

2. Определить `instance Functor` для следующих типов данных:

```haskell
-- Представление бесконечной ленты с текущим положением (как в МТ)
data Tape a = Tape [a] a [a]

-- Функция с окружением
data Reader env a = Reader (env -> a)

data Either a b = Left a | Right b

data Const a b = Const a
```
