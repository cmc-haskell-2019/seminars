# Семинар 09.04.2019

# Моноиды

```haskell
class Monoid a where
  mempty  :: a
  mappend :: a -> a -> a
```

Инфиксный оператор:
```haskell
(<>) = mappend
```

Нейтральный элемент:
```haskell
mempty <> x = x
x <> mempty = x
```

Ассоциативная операция:
```haskell
x <> (y <> z) = (x <> y) <> z
```

Задачи с семинара:

```haskell
-- Внутреннее представление всегда должно быть отсортировано.
newtype SortedList a = SortedList [a]

instance Ord a => Monoid (SortedList a) where
  mempty = SortedList []
  mappend xs (SortedList []) = xs
  mappend (SortedList []) ys = ys
  mappend l1@(SortedList (x : xs)) l2@(SortedList (y : ys))
    | x <= y    = dummyCons x (mappend (SortedList xs) l2)
    | otherwise = dummyCons y (mappend l1 (SortedList ys))
    where
      dummyCons x (SortedList xs) = SortedList (x : xs)

-- Mean a хранит среднее и кол-во значений, из которых это среднее получено.
data Mean a = Mean Int a

instance Fractional a => Monoid (Mean a) where
  mempty = Mean 0 0
  mappend (Mean 0 _) (Mean 0 _) = mempty
  mappend (Mean n1 m1) (Mean n2 m2) =
    Mean (n1 + n2) ((n1'*m1 + n2'*m2)/(n1' + n2'))
    where
      n1' = fromIntegral n1
      n2' = fromIntegral n2

-- Обёртка для Maybe для получения последнего непустого значения
newtype Last a = Last { getLast :: Maybe a }

instance Monoid (Last a) where
  mempty = Last Nothing
  a <> Last Nothing = a
  _ <> b            = b
```

# Свёртки

```haskell
class Foldable f where
  foldMap :: Monoid m => (a -> m) -> f a -> m
```

Задачи с семинара:

```haskell
-- Определите тип двоичного дерева

data BinTree a
  = Empty
  | Node a (BinTree a) (BinTree a)

instance Foldable BinTree where
  foldMap :: Monoid m => (a -> m) -> BinTree a -> m
  foldMap f Empty    = mempty
  foldMap f (Node x l r) = (foldMap f l) <> (f x) <> (foldMap f r)

-- Получить список всех элементов в дереве
elemList :: BinTree a -> [a]
elemList = foldMap (\x -> [x])
-- Здесь foldMap :: (a -> [a]) -> BinTree a -> [a]
```

# Домашнее задание

1. `First` -- обёртка для `Maybe` для получения первого непустого значения:
    ```haskell
    newtype First a = First { getFirst :: Maybe a }
    ```
    Определить для него `instance Monoid`.

2. Реализовать функции `find` и `findLast`, используя моноиды `First` и `Last`
   и функцию `foldMap`. Обе функции имеют тип ` Foldable t => (a -> Bool) -> t a -> Maybe a`.
   `find` возвращает **первый** элемент структуры, удовлетворяющий предикату,
   `findLast` -- **последний** такой элемент.
