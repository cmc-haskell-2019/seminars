module TaskBot
    ( runBot -- this function is exported from module
    ) where

type Task = String
type Command = String

-- | Run task bot.
-- Commands:
-- /list -- show task list
-- /complete -- complete the last task
-- /exit -- stop bot
-- Any other input is considered as new task.
runBot :: IO ()
runBot = do
  putStrLn "Enter your name: "
  name <- getLine
  go name []
  where
    go :: String -> [Task] -> IO ()
    go name tasks = do
      putStr $ name ++ "> "
      s <- getLine -- read a command
      if (s /= "/exit")
        then do
          -- process input unless it is an "/exit" command
          let (output, newTasks) = processCommand s tasks
          putStrLn $ "Bot> " ++ output
          go name newTasks
        else
          putStrLn "Goodbye!"

-- | Process user input. Returns output string to be printed by bot and
-- updated list of tasks in a tuple.
processCommand :: Command -> [Task] -> (String, [Task])
processCommand cmd tasks = case cmd of
  "/list"     -> cmdList tasks
  "/complete" -> cmdComplete tasks
  _           -> newTask cmd tasks

-- | Command to show tasks list.
cmdList :: [Task] -> (String, [Task])
cmdList tasks = (show tasks, tasks) -- TODO: pretty print tasks list

-- | Command to complete the last task.
cmdComplete :: [Task] -> (String, [Task])
cmdComplete [] = ("No tasks!", [])
cmdComplete (_ : rest) = ("Task completed!", rest)

-- | Add new task to tasks list.
newTask :: Task -> [Task] -> (String, [Task])
newTask newT tasks = ("Task added!", (newT : tasks))
