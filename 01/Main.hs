module Main where

import System.IO

import TaskBot

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering -- turn off stdout buffering
  runBot
