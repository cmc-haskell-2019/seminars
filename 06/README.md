# Семинар 26.03.2019

# Классы типов

Несколько примеров полезных классов типов:

1. `Default` -- для указания значения по умолчанию.

    class Default a where
      def :: a


2. `IsString` -- для записи значений в виде строковых литералов:

    {-# LANGUAGE OverloadedStrings #-}
    class IsString a where
      fromString :: String -> a

3. `Listable` -- для типов, представимых в виде списка:

    class Listable a b where
      toList :: a -> [b]
      fromList :: [b] -> a

4. `Collection` -- общий интерфейс для коллекции:

    class Eq e => Collection c e where
      insert :: c -> e -> c
      member :: c -> e -> Bool

5. `Coercible` -- типы, допускающие преобрзование:

    class Coercible a b where
      coerce :: a -> b
