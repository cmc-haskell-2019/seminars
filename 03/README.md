# Семинар 05.03.2019

# Бесконечные структуры данных

Обычный список можно определить так:

```haskell
    data List a
        = Nil             -- пустой список
        | Cons a (List a) -- непустой список (голова и хвост)
```

В Haskell есть удобный синтаксис для списков:

    [a]                           -- список элементов типа a
    (:) :: a -> [a] -> [a]        -- конструктор Cons
    [1 .. 10], [42 ..], [0,2,4..] -- интервалы
    [x, y, z], (x : xs), (x : _)  -- сопоставление

Точечные пары (как в Лиспе) можно представить следующим типом данных:

```haskell
    data Atom = ...
    data LispCons = Atom | Cons Atom LispCons
```

Обе этих струтуры потенциально бесконечны, но могут быть конечными,
поскольку у них есть нужный для этого вариант конструктора значений
(`Nil` и `Atom`) соответственно.

Определим **всегда бесконечный** поток элементов типа `а`:

    data Stream a    =    StreamC a (Stream a)
         \----/           \    /     \----/
       конструктор         \  /    конструктор
          типа              \/        типа
                        конструктор
                          значения

Примеры использования:

```haskell
    ones :: Stream Int
    ones = StreamC 1 ones
```

Задачи, разобранные на семинаре:

1. Реализовать функцию `streamFrom :: Int -> Stream Int`, которая строит
    бесконечный поток чисел, начиная с `n`:

```haskell
    streamFrom :: Int -> Stream Int
    streamFrom n = StreamC n (streamFrom (n + 1))
```

2. Реализовать функцию `takeStream :: Int -> Stream a -> [a]`, которая
    возвращает первые `n` элементов в потоке. Почему возвращаемое
    значение это список, а не поток?

```haskell
    takeStream :: Int -> Stream a -> [a]
    takeStream n (StreamC x xs) | n > 0 = x : (takeStream (n - 1) xs)
                                | otherwise = []
```

3. Реализовать функцию `mapStream :: (a -> b) -> Stream a -> Stream b`.

```haskell
    mapStream :: (a -> b) -> Stream a -> Stream b
    mapStream f (StreamC x xs) = StreamC (f x) (mapStream f xs)
```

4. Реализовать функию `runningAverage :: Fractional a => Stream a -> Stream a`,
    вычисляющую потоковое среднее для каждого элемента последовательности.

```haskell
    runningAverage :: Fractional a => Stream a -> Stream a
    runningAverage s = fun 0 0 s

    fun :: Fractional a => a -> a -> Stream a -> Stream a
    fun avg n (StreamC x xs) = StreamC next (fun next (n + 1) xs)
        where
            next = (avg * n + x) / (n + 1)
```

# Домашнее задание

В файле [Cart.hs](Cart.hs) -- реализация модели данных для корзины в интернет-магазине. [Скринкаст](https://drive.google.com/file/d/1z0uLBwQFoJ5WJuLAexw3-hev0YDgjvDO/view?usp=sharing).

Нужно сделать типы более безопасными:

- использовать `newtype` вместо синонимов типов;
- использовать полноценные структуры данных с именованными
    полями вместо кортежей;
- переписать все функции для подсчёта итоговой стоимости и добитьтся
    отсутствия ошибок при компиляции.

