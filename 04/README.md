# Семинар 12.03.2019

# Абстракция списков (list comprehensions)

В Haskell есть интересные способы работы со списками с использованием
синтаксического сахара. Помимо интервалов `[1..]`, `[10..20]` и т.п.
применяются конструкции вида `[<выражение> | <ограничения> ]`,
где `<выражение>` описывает каждый элемент результирующего списка,
а ограничениями могут служить генераторы вида `x <- l` (будут перебираться
все значения из списка `l`), фильтры (функции типа `Bool`,
определяющие, включать ли очередной элемент в результат) и локальные
связывания с помощью конструкции `let`:

    -- Пример использования генератора
    l1 :: [Int]
    l1 = [ x^2 | x <- [1..4] ]

    -- Генератор -- не обязательно константа
    squares :: [Int] -> [Int]
    squares l = [ x^2 | x <- l ]

    -- Пример использования фильтров
    guard1 :: [Int] -> [Int]
    guard1 l = [ x | x <- l, x > 0, x < 10 ]

    -- Генератор из двух списков + фильтр
    guard2 :: [Int] -> [Int] -> [Int]
    guard2 l1 l2 = [ x*y | x <- l1, y <- l2, x < y ]

    -- Пример локального связывания с помощью let
    let1 :: [Int] -> [Int]
    let1 l = [ y | x <- [1..5]
                 , let l' = map (+ x) l
                 , y <- takeWhile (<10) l'
             ]

Задачи с семинара:

    map' :: (a -> b) -> [a] -> [b]
    map' f l = [ f x | x <- l]

    filter' :: (a -> Bool) -> [a] -> [a]
    filter' f l = [ x | x <- l, f x ]

    cartesian :: [a] -> [b] -> [(a, b)]
    cartesian l1 l2 = [ (x, y) | x <- l1, y <- l2 ]

    pythTriples :: [(Int, Int, Int)]
    pythTriples = [ (x,y,z) | z <- [1..]
                            , y <- [1..z]
                            , x <- [1..y]
                            , x^2+y^2 == z^2 ]


    primes :: [Int]
    primes = [ round x | x <- [2..]
                       , let l = [2..(ceiling (sqrt x))]
                             mods = map (mod (round x)) l
                       , all (/=0) mods ]

# Расширения компилятора

Расширения компилятора позволяет использовать нестандартные
конструкции, которые недоступны по умолчанию, но могут быть удобны:

Мы рассмотрели следующие расширения:

- ScopedTypeVariables
- RecordWildCards
- GeneralizedNewtypeDeriving
- TupleSections
- LambdaCase

С документацией этих и других расширений можно ознакомиться
по [ссылке](https://downloads.haskell.org/~ghc/8.4.2/docs/html/users_guide/glasgow_exts.html).

Примеры использования -- в файле [Exts.hs](Exts.hs). [Скринкаст](https://drive.google.com/file/d/1zbljOwUQ3QERS6IuhrP48Y_SB4h3A7p9/view?usp=sharing).
